package test.adf.global.beans;

import javax.faces.event.ActionEvent;
import oracle.adf.share.ADFContext;
import oracle.adfinternal.controller.state.PageFlowScope;
import test.adf.global.utils.ScopeUtils;
import test.adf.global.utils.TaskFlowUtils;

public class MainBean {
    public MainBean() {
    }

    private String name = "main";

    public void setName(String name) {
      this.name = name;
    }

    public String getName() {
      return name;
    }

    public void dumpChildPageFlowScope(ActionEvent actionEvent) {
        
        System.out.println();
        System.out.println("[ WRITE VALUES TO PAGEFLOWSCOPE ]");
        System.out.println();
        
        TaskFlowUtils.printTaskFlowInformation();
        ScopeUtils.printPageFlowScope();
        
        System.out.println();
        System.out.println("  [ METHOD 1 ]");
        System.out.println();
        
        method1();
        
        System.out.println();
        System.out.println("__________________");
        
        System.out.println();
        System.out.println("  [ METHOD 2 ]");
        System.out.println();
        
        method2();
        
        System.out.println();
        System.out.println("__________________");
        
        System.out.println();
        System.out.println("  [ METHOD 3 ]");
        System.out.println();
        
        method3();
                  
        System.out.println();
        System.out.println("[ WRITE VALUES TO PAGEFLOWSCOPE ]");
        System.out.println();
        
    }
    
    
    private void method1(){
        PageFlowScope pfs = ScopeUtils.getPageFlowScopeByChildView("child1");
        pfs.put("myKey1", "myValue1");
        // ScopeUtils.printPageFlowScope(pfs);
        
    }
    
    private void method2(){
        PageFlowScope pfs = ScopeUtils.getPageFlowScopeByViewPortClientId("data.test_adf_global_view_mainViewPageDef.child1");
        pfs.put("myKey2", "myValue2");
        // ScopeUtils.printPageFlowScope(pfs);
    }
    
    private void method3(){
        PageFlowScope pfs = ScopeUtils.getPageFlowScopeByTaskFlowId("/WEB-INF/child.xml#child");
        pfs.put("myKey3", "myValue3");
        // ScopeUtils.printPageFlowScope(pfs);
    }
    
  
} // The End of Class;
