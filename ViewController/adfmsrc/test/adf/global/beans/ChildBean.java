package test.adf.global.beans;

import javax.faces.event.ActionEvent;
import test.adf.global.interfaces.BeanInt;
import test.adf.global.utils.ScopeUtils;
import test.adf.global.utils.TaskFlowUtils;

public class ChildBean implements BeanInt {
    public ChildBean() {
    }

    private String name = "child";

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
    public void runTest(ActionEvent actionEvent) {
        
        System.out.println();
        System.out.println("[ PRINT RESULT ] ");
        System.out.println();
        
        TaskFlowUtils.printTaskFlowInformation();
        ScopeUtils.printPageFlowScope();
        
        System.out.println();
        System.out.println("[ PRINT RESULT ] ");
        System.out.println();
    }
    

} // The End of Class;
