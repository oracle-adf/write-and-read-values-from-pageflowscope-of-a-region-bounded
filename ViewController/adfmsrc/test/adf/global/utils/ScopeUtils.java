package test.adf.global.utils;

import java.util.Map;
import java.util.Set;

import oracle.adf.controller.internal.binding.DCTaskFlowBinding;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adfinternal.controller.state.PageFlowScope;

import test.adf.global.utils.impl.GetPageFlowScopeMethod1;
import test.adf.global.utils.impl.GetPageFlowScopeMethod2;
import test.adf.global.utils.impl.GetPageFlowScopeMethod3;


public class ScopeUtils {
    
    
    public static void printPageFlowScope(){
        
        AdfFacesContext adfFacesContext = null;
        adfFacesContext = AdfFacesContext.getCurrentInstance();
        // Map _pageFlowScope = adfFacesContext.getPageFlowScope();
        
        printPageFlowScope((PageFlowScope)adfFacesContext.getPageFlowScope());
    }
    
    
    public static void printPageFlowScope(PageFlowScope pageFlowScope){
                
        Set<Map.Entry<String, Object>> entrySet = pageFlowScope.entrySet();
        
        System.out.println();
        System.out.println("  [ PAGE FLOW SCOPE ]");
        
        for (Map.Entry<String, Object> entry : entrySet) {
            String key = entry.getKey();
            Object value = entry.getValue();
            
            System.out.println("    [key] " + key + ", [value] " + value.toString());
        }
        
        System.out.println("  [ PAGE FLOW SCOPE ]");
        System.out.println();
    }
    
    // ------------------------------------------------------
    
    public static PageFlowScope getPageFlowScopeByChildView(String taskFlowId){
        return GetPageFlowScopeMethod1.getPageFlowScope(taskFlowId);
    }
    
    public static PageFlowScope getPageFlowScopeByViewPortClientId(String viewPortClientId){
        return GetPageFlowScopeMethod2.getPageFlowScope(viewPortClientId);
    }
    
    public static PageFlowScope getPageFlowScopeByTaskFlowId(String taskFlowId){
        return GetPageFlowScopeMethod3.getPageFlowScopeByTaskFlowId(taskFlowId);
    }
    
   
    
    
} // The End of Class;
