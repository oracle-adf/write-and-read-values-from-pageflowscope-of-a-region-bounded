package test.adf.global.utils.impl;

import oracle.adf.controller.ControllerContext;
import oracle.adf.controller.ViewPortContext;

import oracle.adfinternal.controller.state.AdfcContext;
import oracle.adfinternal.controller.state.ChildViewPortContextImpl;
import oracle.adfinternal.controller.state.PageFlowScope;
import oracle.adfinternal.controller.state.RootViewPortContextImpl;

public class GetPageFlowScopeMethod2 {
    
    public static PageFlowScope getPageFlowScope(String viewPortClientId){
        
        ChildViewPortContextImpl childView = getChildViewByTaskFlowId(viewPortClientId);
        
        System.out.println("  child view " + childView.getViewId());
        
        System.out.println();
        
        System.out.println("  childView.getInitialTaskFlowId() " + childView.getInitialTaskFlowId());
        
        AdfcContext afdcContext = AdfcContext.getCurrentInstance();
        return childView.getPageFlowScopeMap(afdcContext);

    }
    
    
    // data.test_adf_global_view_mainViewPageDef.child1
    private static ChildViewPortContextImpl getChildViewByTaskFlowId(String viewPortClientId){
      ControllerContext conn = ControllerContext.getInstance(); 
      RootViewPortContextImpl rootViewPort = (RootViewPortContextImpl)conn.getCurrentRootViewPort(); 
      return (ChildViewPortContextImpl)rootViewPort.getChildViewPortByClientId(viewPortClientId); 
    }
    
    
    

} // The End of Class;
