package test.adf.global.utils.impl;

import oracle.adf.controller.ControllerContext;
import oracle.adf.controller.ViewPortContext;
import oracle.adf.controller.internal.binding.DCTaskFlowBinding;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;

import oracle.adfinternal.controller.state.AdfcContext;
import oracle.adfinternal.controller.state.ChildViewPortContextImpl;
import oracle.adfinternal.controller.state.PageFlowScope;
import oracle.adfinternal.controller.state.RootViewPortContextImpl;

import test.adf.global.utils.ScopeUtils;

public class GetPageFlowScopeMethod1 {
    
    
    // Method 1
    
    public static PageFlowScope getPageFlowScope(String taskFlowId){

        ChildViewPortContextImpl childViewPortContextImpl = getChildView(taskFlowId);
       
        System.out.println("  child view " + childViewPortContextImpl.getViewId());
        
        System.out.println();
        
        System.out.println("  childView.getInitialTaskFlowId() " + childViewPortContextImpl.getInitialTaskFlowId());
        
        AdfcContext afdcContext = AdfcContext.getCurrentInstance();
        return childViewPortContextImpl.getPageFlowScopeMap(afdcContext);

    }
    
    private static ChildViewPortContextImpl getChildView(String taskFlowId){
        
      // find the task flow pagedef binding, see the pageDef 
      
      ControllerContext conn = ControllerContext.getInstance(); 
      RootViewPortContextImpl rootViewPort = (RootViewPortContextImpl)conn.getCurrentRootViewPort(); 
      
      DCTaskFlowBinding dcTfb = getDCTaskFlowBinding(taskFlowId);
      
        System.out.println();
        System.out.println();
        System.out.println("  DCTaskFlowBinding FullName " + dcTfb.getFullName());
        System.out.println();
        System.out.println();

      return (ChildViewPortContextImpl)rootViewPort.getChildViewPortByClientId(dcTfb.getFullName()); 
    }
    
    // ------------------------------------------------------
    
    public static DCTaskFlowBinding getDCTaskFlowBinding(String taskFlowId){
        
        // get the current BindingContainer 
          
        BindingContext bctx = BindingContext.getCurrent(); 
        DCBindingContainer mainViewPageBinding = (DCBindingContainer)bctx.getCurrentBindingsEntry();
        
        DCTaskFlowBinding tf = (DCTaskFlowBinding)mainViewPageBinding.findExecutableBinding(taskFlowId); 
        return tf;
    }
    
} // The End of Class;
