package test.adf.global.utils.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.context.FacesContext;

import oracle.adf.controller.ControllerContext;
import oracle.adf.controller.internal.ViewPortContextFwk;

import oracle.adfinternal.controller.ControllerContextImpl;
import oracle.adfinternal.controller.state.AdfcContext;
import oracle.adfinternal.controller.state.ChildViewPortContextImpl;
import oracle.adfinternal.controller.state.PageFlowScope;
import oracle.adfinternal.controller.state.RootViewPortContextImpl;

import test.adf.global.utils.ScopeUtils;

public class GetPageFlowScopeMethod3 {
    
    //Main Method:
    public static PageFlowScope getPageFlowScopeByTaskFlowId(String taskFlowId){

        Map tfMap = new HashMap();

        ControllerContext conn = ControllerContext.getInstance(); 
        RootViewPortContextImpl rootViewPort = (RootViewPortContextImpl)conn.getCurrentRootViewPort();
        
        Set clientIds = (Set)rootViewPort.getChildViewPortClientIds();
        
                for(Object id : clientIds){
                    
                    ViewPortContextFwk fwk = rootViewPort.getChildViewPortByClientId(id.toString());
                    System.out.println(" [fwk] " + fwk);
                    
                    String tfId = fwk.getTaskFlowContext().getTaskFlowId().getFullyQualifiedName();
                    System.out.println(" [tfId] " + tfId);
                    
                    tfMap.put(tfId,id.toString());
                }
                
//                System.out.println(" INPUT " + taskFlowId);
//                System.out.println();
//                printInfoTfMap(tfMap);
        
        ChildViewPortContextImpl viewPort = getViewPortByClientId(tfMap.get(taskFlowId).toString());
        
        AdfcContext afdcContext = AdfcContext.getCurrentInstance();
        return viewPort.getPageFlowScopeMap(afdcContext);
        
    }
    
    private static void printInfoTfMap(Map tfMap){
                Set<Map.Entry<String, Object>> entrySet = tfMap.entrySet();
                
                for (Map.Entry<String, Object> entry : entrySet) {
                    String key = entry.getKey();
                    Object value = entry.getValue();
                    
                    System.out.println("[key] " + key );
                    System.out.println("[value] " + value.toString());
                }
    }

    // Helper Method:
    // data.test_adf_global_view_mainViewPageDef.child1
    private static ChildViewPortContextImpl getViewPortByClientId(String viewPortClientId){
        Object o1 = resolveExpression("#{controllerContext}");
        ControllerContextImpl ctx = (ControllerContextImpl)o1;
        return (ChildViewPortContextImpl)ctx.getViewPortByClientId(viewPortClientId);
    }
    
    public static Object resolveExpression(String expression) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        Application app = facesContext.getApplication();
        ExpressionFactory elFactory = app.getExpressionFactory();
        ELContext elContext = facesContext.getELContext();
        ValueExpression valueExp =
            elFactory.createValueExpression(elContext, expression,
                                            Object.class);
        return valueExp.getValue(elContext);
    }

} // The End of Class;
